package com.legacy.surrounding_indicators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.surrounding_indicators.client.IndicatorClient;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.IExtensionPoint;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.network.NetworkConstants;

@Mod(SurroundingIndicatorsMod.MODID)
public class SurroundingIndicatorsMod
{
	public static final String MODID = "surrounding_indicators";

	public static final Logger LOGGER = LogManager.getLogger("ModdingLegacy/" + MODID);
	public static final MLSupporter SUPPORTERS = new MLSupporter(MODID);

	public SurroundingIndicatorsMod()
	{
		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, IndicatorConfig.CLIENT_SPEC);

			var modBus = FMLJavaModLoadingContext.get().getModEventBus();
			modBus.addListener((FMLClientSetupEvent event) -> SurroundingIndicatorsMod.SUPPORTERS.refresh());
			modBus.addListener(IndicatorClient::registerOverlays);
		});

		ModLoadingContext.get().registerExtensionPoint(IExtensionPoint.DisplayTest.class, () -> new IExtensionPoint.DisplayTest(() -> NetworkConstants.IGNORESERVERONLY, (a, b) -> true));

		DistExecutor.safeRunWhenOn(Dist.DEDICATED_SERVER, () -> () -> LOGGER.warn("SURROUNDING INDICATORS IS A CLIENT-SIDE MOD. IT DOES NOT NEED TO BE INSTALLED ON THE SERVER."));
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return new String(MODID + ":" + name);
	}
}
