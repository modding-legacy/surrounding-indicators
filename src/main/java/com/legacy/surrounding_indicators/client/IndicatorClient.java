package com.legacy.surrounding_indicators.client;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import com.legacy.surrounding_indicators.IndicatorConfig;
import com.legacy.surrounding_indicators.MLSupporter.Rank;
import com.legacy.surrounding_indicators.SurroundingIndicatorsMod;
import com.legacy.surrounding_indicators.client.render.DamageIndicatorRenderer;
import com.mojang.blaze3d.platform.Window;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraftforge.client.event.RegisterGuiOverlaysEvent;

@SuppressWarnings("resource")
public class IndicatorClient
{
	private static Minecraft mc()
	{
		return Minecraft.getInstance();
	}

	public static void registerOverlays(RegisterGuiOverlaysEvent event)
	{
		event.registerAboveAll(SurroundingIndicatorsMod.find("health_overlay").replace(":", "_"), (gui, mStack, partialTicks, screenWidth, screenHeight) -> onOverlayEvent(mStack, screenWidth, screenHeight));
	}

	// @SubscribeEvent
	public static void onOverlayEvent(PoseStack stack, int screenWidth, int screenHeight)
	{
		if (/*event.getType() == ElementType.ALL && */!mc().options.renderDebug)
		{
			Player player = mc().player;

			if (player == null || player != null && player.level == null)
				return;

			Level world = player.level;
			// Window res = mc().getWindow();

			try
			{
				List<Entity> entities = world.getEntities(player, player.getBoundingBox().inflate(IndicatorConfig.indicatorDistance()), NOT_SPECTATING_AND_LIVING.and(ENTITY_VISIBLE));
				List<Entity> players = world.getEntities(player, player.getBoundingBox().inflate(IndicatorConfig.indicatorDistance()), IS_PLAYER.and(ENTITY_VISIBLE));
				List<Entity> non_teamed_players = world.getEntities(player, player.getBoundingBox().inflate(IndicatorConfig.indicatorDistance()), IS_PLAYER.and(ENTITY_NOT_ON_SAME_TEAM).and(ENTITY_VISIBLE));
				List<Entity> non_teamed_entities = world.getEntities(player, player.getBoundingBox().inflate(IndicatorConfig.indicatorDistance()), NOT_SPECTATING_AND_LIVING.and(ENTITY_NOT_ON_SAME_TEAM).and(ENTITY_VISIBLE));
				List<Entity> team_entities = world.getEntities(player, player.getBoundingBox().inflate(mc().options.getEffectiveRenderDistance() * 16, 64.0D, mc().options.getEffectiveRenderDistance() * 16), NOT_SPECTATING_AND_LIVING.and(ENTITY_ON_SAME_TEAM));

				int normalIndicatorX = IndicatorConfig.swapIndicatorSides() ? screenWidth - IndicatorConfig.rightIndicatorX() : IndicatorConfig.leftIndicatorX();
				int teamIndicatorX = !IndicatorConfig.swapIndicatorSides() ? screenWidth - IndicatorConfig.rightIndicatorX() : IndicatorConfig.leftIndicatorX();
				int normalIndicatorY = IndicatorConfig.swapIndicatorSides() ? IndicatorConfig.rightIndicatorY() : IndicatorConfig.leftIndicatorY();
				int teamIndicatorY = !IndicatorConfig.swapIndicatorSides() ? IndicatorConfig.rightIndicatorY() : IndicatorConfig.leftIndicatorY();

				if (IndicatorConfig.onlyPlayerIndicators())
				{
					if (IndicatorConfig.enableTeamIndicators())
						beginHealthRendering(stack, non_teamed_players, normalIndicatorX, normalIndicatorY, false);
					else
						beginHealthRendering(stack, players, normalIndicatorX, normalIndicatorY, false);
				}
				else
				{
					if (IndicatorConfig.enableTeamIndicators())
						beginHealthRendering(stack, non_teamed_entities, normalIndicatorX, normalIndicatorY, false);
					else
						beginHealthRendering(stack, entities, normalIndicatorX, normalIndicatorY, false);
				}

				if (IndicatorConfig.enableTeamIndicators())
					beginHealthRendering(stack, team_entities, teamIndicatorX, teamIndicatorY, true);

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private static void beginHealthRendering(PoseStack stack, List<Entity> entities, int x, int y, boolean team)
	{
		Window res = mc().getWindow();

		if (!entities.isEmpty())
		{
			entities.sort(CLOSEST_WITH_BULK);

			int currentY = y;
			for (int i = 0; i < entities.size(); ++i)
			{
				Entity entity = entities.get(i);
				if (entity != null && entity instanceof LivingEntity living && (team ? i < IndicatorConfig.maxTeamIndicatorsDisplayed() : i < IndicatorConfig.maxIndicatorsDisplayed()))
				{
					String childModifier = living.isBaby() ? "Baby " : "";
					String numericalIndicator = IndicatorConfig.enableNumericalIndicator() ? Mth.ceil(living.getHealth()) + "/" + Mth.ceil(living.getMaxHealth()) : "";

					int maxHealth = (int) (living.getMaxHealth());
					int rowCount = maxHealth / 20 + (maxHealth % 20 > 0 ? 1 : 0);

					Component name = living.getDisplayName();

					Rank rank;
					if (living instanceof Player player && ((rank = SurroundingIndicatorsMod.SUPPORTERS.getRank(player)).hasPerks()))
					{
						ChatFormatting bonusColor = rank.isDeveloper() ? ChatFormatting.LIGHT_PURPLE : ChatFormatting.GOLD;
						name = Component.literal(living.getDisplayName().getString()).withStyle(bonusColor);
					}

					Component finalName = Component.literal(childModifier).append(name).append(" " + numericalIndicator);
					mc().font.drawShadow(stack, finalName, 2 + x + (x > res.getGuiScaledWidth() / 2 ? -(mc().font.width(finalName.getString()) - 90) : 0), 2 + currentY, -1);
					DamageIndicatorRenderer.renderHealth(stack, living, res.getGuiScaledWidth(), res.getGuiScaledHeight(), x, currentY);

					if (maxHealth > 50)
						currentY += (rowCount) * 3 + 20;
					else
						currentY += (rowCount) * 10 + 12;
				}
			}
		}
	}

	private static final Comparator<Entity> CLOSEST_WITH_BULK = new Comparator<Entity>()
	{
		@Override
		public int compare(Entity p1, Entity p2)
		{
			int health = 0;
			int dist = 0;

			if (p1 instanceof LivingEntity l1 && p2 instanceof LivingEntity l2)
				health = (int) (l2.getMaxHealth() - l1.getMaxHealth());

			if (mc().player != null)
				dist = (int) (p1.distanceTo(mc().player) - p2.distanceTo(mc().player));

			return dist + health;
		}
	};

	public static final Predicate<Entity> NOT_SPECTATING_AND_LIVING = (entity) ->
	{
		// TODO check for bosses
		return entity.isAlive() && !entity.isSpectator() && entity instanceof LivingEntity && !(entity instanceof ArmorStand)/* && entity.getType().is(Tags.Entity)*/ && !entity.isInvulnerable() && !entity.getDisplayName().getString().contains("click") && !entity.getDisplayName().getString().contains("join");
	};

	public static final Predicate<Entity> IS_PLAYER = (entity) ->
	{
		return NOT_SPECTATING_AND_LIVING.test(entity) && entity instanceof Player;
	};

	public static final Predicate<Entity> ENTITY_ON_SAME_TEAM = (entity) ->
	{
		return entity.isAlliedTo(mc().player) && !(entity instanceof TamableAnimal) || IndicatorConfig.enableTamedAllies() && entity instanceof TamableAnimal tamed && tamed.isOwnedBy(mc().player);
	};

	public static final Predicate<Entity> ENTITY_NOT_ON_SAME_TEAM = (entity) ->
	{
		return !ENTITY_ON_SAME_TEAM.test(entity);
	};

	public static final Predicate<Entity> ENTITY_VISIBLE = (entity) ->
	{
		if (entity instanceof LivingEntity living && living.hasEffect(MobEffects.INVISIBILITY))
			return false;

		return !entity.isCrouching() && !entity.isInvisible();
	};
}
