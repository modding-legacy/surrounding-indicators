package com.legacy.surrounding_indicators.client.render;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobType;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.animal.horse.AbstractHorse;
import net.minecraft.world.entity.raid.Raider;

public class DamageIndicatorRenderer
{
	public static Minecraft mc = Minecraft.getInstance();
	private static final ResourceLocation ICONS = new ResourceLocation("textures/gui/icons.png");

	private static int right_height = 39;

	public static void renderHealth(PoseStack stack, LivingEntity entity, int width, int height, int extraWidth, int extraHeight)
	{
		if (entity != null)
		{
			RenderSystem.setShaderTexture(0, ICONS);
			stack.pushPose();
			RenderSystem.enableBlend();
			stack.translate(0.0F, 0.0F, 0.0F);

			int health = Mth.ceil(entity.getHealth());

			AttributeInstance attrMaxHealth = entity.getAttribute(Attributes.MAX_HEALTH);

			float healthMax = (float) attrMaxHealth.getValue();
			/*float absorb = Mth.ceil(entity.getAbsorptionAmount());
			
			int healthRows = Mth.ceil((healthMax + absorb) / 2.0F / 1.0F);
			int rowHeight = Math.max(10 - (healthRows - 2), 3);*/
			int hearts = (int) (healthMax + 0.5F) / 2;

			// rand.setSeed(entity.tickCount * 312871);

			int left = 5 + extraWidth;
			right_height = 12 + extraHeight;

			int textureY = 0;

			if (entity.getType() == EntityType.ELDER_GUARDIAN || entity.getType() == EntityType.WARDEN || entity.getType() == EntityType.WITHER || entity.getType() == EntityType.ENDER_DRAGON)
				textureY = 45;

			final int background = 16;

			boolean poison = /*entity.hasEffect(MobEffects.POISON) || */entity.getType() == EntityType.HUSK;
			boolean wither = /*entity.hasEffect(MobEffects.WITHER) || */entity.getType() == EntityType.WITHER || entity.getType() == EntityType.WITHER_SKELETON;
			boolean frozen = entity.isFullyFrozen() || entity.getType() == EntityType.STRAY;
			boolean illager = entity instanceof Raider || entity.getMobType() == MobType.ILLAGER || entity.getType() == EntityType.VEX;

			boolean mount = !frozen && (entity instanceof AbstractHorse || entity.getType() == EntityType.STRIDER);

			// 52 poison, 142 frozen, 34 pink, gold 124
			int textureX = poison ? 52 : wither ? 106 : frozen ? 142 : illager ? 34 : mount ? 52 : 16;

			for (int heart = 0; hearts > 0; heart += 20)
			{
				int top = right_height;
				int rowCount = Math.min(hearts, 10);
				hearts -= rowCount;

				for (int i = 0; i < rowCount; ++i)
				{
					int extra = 0;
					/*if (entity.getRandom().nextBoolean() && entity.tickCount % 2 == 0 && entity.getHealth() <= (entity.getMaxHealth() / 5))
						extra += rand.nextInt(4) - 2;*/

					int x = left + i % (int) healthMax * 8;
					draw(stack, x, top + extra, background, textureY, 9, 9);

					/*if (mount)
						TOP = 10;*/

					if (i * 2 + 1 + heart < health)
						draw(stack, x, top + extra, textureX + 36, mount ? 9 : textureY, 9, 9);
					else if (i * 2 + 1 + heart == health)
					{
						if (mount)
							draw(stack, x, top + extra, textureX + 36, 9, 5, 9);
						else
							draw(stack, x, top + extra, textureX + 45, textureY, 9, 9);
					}
				}

				right_height += healthMax > 50 ? 3 : 10;
			}

			Minecraft.getInstance().getProfiler().pop();
			RenderSystem.disableBlend();
			stack.popPose();
		}
	}

	private static void draw(PoseStack stack, int x, int y, int textureX, int textureY, int width, int height)
	{
		Gui.blit(stack, x, y, textureX, textureY, width, height, 256, 256);
	}
}
