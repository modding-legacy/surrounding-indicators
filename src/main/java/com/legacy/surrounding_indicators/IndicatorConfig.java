package com.legacy.surrounding_indicators;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;

public class IndicatorConfig
{
	public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ClientConfig CLIENT;
	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> clientPair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = clientPair.getLeft();
			CLIENT_SPEC = clientPair.getRight();
		}
	}

	private static String translate(String key)
	{
		return key;
	}

	public static class ClientConfig
	{
		public final ForgeConfigSpec.ConfigValue<Double> indicatorDistance;
		public final ForgeConfigSpec.ConfigValue<Integer> maxIndicatorsDisplayed;
		public final ForgeConfigSpec.ConfigValue<Boolean> onlyPlayerIndicators;
		public final ForgeConfigSpec.ConfigValue<Boolean> enableTeamIndicators;
		public final ForgeConfigSpec.ConfigValue<Integer> maxTeamIndicatorsDisplayed;
		public final ForgeConfigSpec.ConfigValue<Boolean> enableNumericalIndicator;
		public final ForgeConfigSpec.ConfigValue<Boolean> enableTamedAllies;

		public final ForgeConfigSpec.ConfigValue<Integer> leftIndicatorX;
		public final ForgeConfigSpec.ConfigValue<Integer> leftIndicatorY;
		public final ForgeConfigSpec.ConfigValue<Integer> rightIndicatorX;
		public final ForgeConfigSpec.ConfigValue<Integer> rightIndicatorY;
		public final ForgeConfigSpec.ConfigValue<Boolean> swapIndicatorSides;

		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Surrounding Indicators Client Config").push("client");
			indicatorDistance = builder.translation(translate("indicatorDistance")).comment("Distance the health indicator will detect entities.").define("indicatorDistance", 7.0D);
			maxIndicatorsDisplayed = builder.translation(translate("maxIndicatorsDisplayed")).comment("Max amount of health indicators that will be displayed.").define("maxIndicatorsDisplayed", 5);
			onlyPlayerIndicators = builder.translation(translate("onlyPlayerIndicators")).comment("Determines weather or not only player health indicators should show.").define("onlyPlayerIndicators", false);
			enableTeamIndicators = builder.translation(translate("enableTeamIndicators")).comment("Determines weather or not only team health indicators should show.").define("enableTeamIndicators", true);
			maxTeamIndicatorsDisplayed = builder.translation(translate("maxTeamIndicatorsDisplayed")).comment("Max amount of team health indicators that will be displayed.").define("maxTeamIndicatorsDisplayed", 7);
			enableNumericalIndicator = builder.translation(translate("enableNumericalIndicator")).comment("Enables the numerical indicators alongside the mob name.").define("enableNumericalIndicator", true);
			enableTamedAllies = builder.translation(translate("enableTamedAllies")).comment("Determines if you will see the health of your tamed mobs, as if they were on your team. (Requires team indicators to be on.)").define("enableTamedAllies", true);

			leftIndicatorX = builder.translation(translate("leftIndicatorX")).comment("The starting point for the left indicator on the x axis. (normal indicator by default)").define("leftIndicatorX", 0);
			leftIndicatorY = builder.translation(translate("leftIndicatorY")).comment("The starting point for the left indicator on the y axis.").define("leftIndicatorY", 0);
			rightIndicatorX = builder.translation(translate("rightIndicatorX")).comment("The starting point for the right indicator on the x axis. (team indicator by default)").define("rightIndicatorX", 95);
			rightIndicatorY = builder.translation(translate("rightIndicatorY")).comment("The starting point for the right indicator on the y axis.").define("rightIndicatorY", 0);
			swapIndicatorSides = builder.translation(translate("swapIndicatorSides")).comment("Swaps the sides the indicators are on, team indicator moves to the left, while the normal one moves right.").define("swapIndicatorSides", false);

			builder.pop();
		}
	}

	public static double indicatorDistance()
	{
		return CLIENT.indicatorDistance.get();
	}

	public static int maxIndicatorsDisplayed()
	{
		return CLIENT.maxIndicatorsDisplayed.get();
	}

	public static boolean onlyPlayerIndicators()
	{
		return CLIENT.onlyPlayerIndicators.get();
	}

	public static boolean enableTeamIndicators()
	{
		return CLIENT.enableTeamIndicators.get();
	}

	public static int maxTeamIndicatorsDisplayed()
	{
		return CLIENT.maxTeamIndicatorsDisplayed.get();
	}

	public static boolean enableNumericalIndicator()
	{
		return CLIENT.enableNumericalIndicator.get();
	}

	public static boolean enableTamedAllies()
	{
		return CLIENT.enableTamedAllies.get();
	}

	public static int leftIndicatorX()
	{
		return CLIENT.leftIndicatorX.get();
	}

	public static int leftIndicatorY()
	{
		return CLIENT.leftIndicatorY.get();
	}

	public static int rightIndicatorX()
	{
		return CLIENT.rightIndicatorX.get();
	}

	public static int rightIndicatorY()
	{
		return CLIENT.rightIndicatorY.get();
	}

	public static boolean swapIndicatorSides()
	{
		return CLIENT.swapIndicatorSides.get();
	}

	/*public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;
	
	public static double indicatorDistance;
	public static int maxIndicatorsDisplayed;
	public static boolean onlyPlayerIndicators;
	public static boolean enableTeamIndicators;
	public static int maxTeamIndicatorsDisplayed;
	public static boolean enableNumericalIndicator;
	public static boolean enableOwnWolves;
	
	public static int leftIndicatorX;
	public static int leftIndicatorY;
	public static int rightIndicatorX;
	public static int rightIndicatorY;
	public static boolean swapIndicatorSides;
	
	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
	}
	
	
	
	private static class ServerConfig
	{
		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Server side changes.").push("client");
			builder.pop();
		}
	}
	
	private static class ClientConfig
	{
		public final ForgeConfigSpec.ConfigValue<Double> indicatorDistance;
		public final ForgeConfigSpec.ConfigValue<Integer> maxIndicatorsDisplayed;
		public final ForgeConfigSpec.ConfigValue<Boolean> onlyPlayerIndicators;
		public final ForgeConfigSpec.ConfigValue<Boolean> enableTeamIndicators;
		public final ForgeConfigSpec.ConfigValue<Integer> maxTeamIndicatorsDisplayed;
		public final ForgeConfigSpec.ConfigValue<Boolean> enableNumericalIndicator;
		public final ForgeConfigSpec.ConfigValue<Boolean> enableOwnWolves;
	
		public final ForgeConfigSpec.ConfigValue<Integer> leftIndicatorX;
		public final ForgeConfigSpec.ConfigValue<Integer> leftIndicatorY;
		public final ForgeConfigSpec.ConfigValue<Integer> rightIndicatorX;
		public final ForgeConfigSpec.ConfigValue<Integer> rightIndicatorY;
		public final ForgeConfigSpec.ConfigValue<Boolean> swapIndicatorSides;
	
		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Client side changes.").push("common");
			indicatorDistance = builder.translation(translate("indicatorDistance")).comment("Distance the health indicator will detect entities.").define("indicatorDistance", 7.0D);
			maxIndicatorsDisplayed = builder.translation(translate("maxIndicatorsDisplayed")).comment("Max amount of health indicators that will be displayed.").define("maxIndicatorsDisplayed", 5);
			onlyPlayerIndicators = builder.translation(translate("onlyPlayerIndicators")).comment("Determines weather or not only player health indicators should show.").define("onlyPlayerIndicators", false);
			enableTeamIndicators = builder.translation(translate("enableTeamIndicators")).comment("Determines weather or not only team health indicators should show.").define("enableTeamIndicators", true);
			maxTeamIndicatorsDisplayed = builder.translation(translate("maxTeamIndicatorsDisplayed")).comment("Max amount of team health indicators that will be displayed.").define("maxTeamIndicatorsDisplayed", 7);
			enableNumericalIndicator = builder.translation(translate("enableNumericalIndicator")).comment("Enables the numerical indicators alongside the mob name.").define("enableNumericalIndicator", true);
			enableOwnWolves = builder.translation(translate("enableOwnWolves")).comment("Determines if you will see the health of your wolves, as if they were on your team. (Requires team indicators to be on.)").define("enableOwnWolves", true);
	
			leftIndicatorX = builder.translation(translate("leftIndicatorX")).comment("The starting point for the left indicator on the x axis. (normal indicator by default)").define("leftIndicatorX", 0);
			leftIndicatorY = builder.translation(translate("leftIndicatorY")).comment("The starting point for the left indicator on the y axis.").define("leftIndicatorY", 0);
			rightIndicatorX = builder.translation(translate("rightIndicatorX")).comment("The starting point for the right indicator on the x axis. (team indicator by default)").define("rightIndicatorX", 95);
			rightIndicatorY = builder.translation(translate("rightIndicatorY")).comment("The starting point for the right indicator on the y axis.").define("rightIndicatorY", 0);
			swapIndicatorSides = builder.translation(translate("swapIndicatorSides")).comment("Swaps the sides the indicators are on, team indicator moves to the left, while the normal one moves right.").define("swapIndicatorSides", false);
	
			builder.pop();
		}
	}
	
	@SuppressWarnings("unused")
	private static class ConfigBakery
	{
		private static ModConfig clientConfig;
		private static ModConfig serverConfig;
	
		public static void bakeClient(ModConfig config)
		{
			clientConfig = config;
	
			indicatorDistance = CLIENT.indicatorDistance.get();
			maxIndicatorsDisplayed = CLIENT.maxIndicatorsDisplayed.get();
			onlyPlayerIndicators = CLIENT.onlyPlayerIndicators.get();
			enableTeamIndicators = CLIENT.enableTeamIndicators.get();
			maxTeamIndicatorsDisplayed = CLIENT.maxTeamIndicatorsDisplayed.get();
			enableNumericalIndicator = CLIENT.enableNumericalIndicator.get();
			enableOwnWolves = CLIENT.enableOwnWolves.get();
	
			leftIndicatorX = CLIENT.leftIndicatorX.get();
			leftIndicatorY = CLIENT.leftIndicatorY.get();
			rightIndicatorX = CLIENT.rightIndicatorX.get();
			rightIndicatorY = CLIENT.rightIndicatorY.get();
			swapIndicatorSides = CLIENT.swapIndicatorSides.get();
		}
	
		public static void bakeServer(ModConfig config)
		{
			serverConfig = config;
		}
	}*/
}
